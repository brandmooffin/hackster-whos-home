﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using WirelessBootService.Models;

namespace WirelessBootService.Data
{
    public class WirelessBootDataManager
    {
        public static bool AddDevice(Device device)
        {
            try
            {
                using (var dbContext = new WirelessBootEntities())
                {
                    // does device fully exist?
                    var deviceFound = dbContext.WirelessBoot_Devices.FirstOrDefault(x => string.Equals(x.DeviceIpAddress, device.IpAddress) && string.Equals(x.DeviceMacAddress, device.MacAddress));
                    // if yes, update IsHome to true
                    if (deviceFound != null)
                    {
                        device.IsHome = true;
                        dbContext.SaveChanges();
                        return true;
                    }

                    deviceFound = dbContext.WirelessBoot_Devices.FirstOrDefault(x => string.Equals(x.DeviceMacAddress, device.MacAddress));
                    // if no, does mac or ip exist?
                    // if mac exists, does the ip belong to someone else?
                    if (deviceFound != null)
                    {
                        // if ip found, remove device
                        var deviceWithIpFound = dbContext.WirelessBoot_Devices.FirstOrDefault(x => string.Equals(x.DeviceIpAddress, device.IpAddress));
                        if (deviceWithIpFound != null)
                        {
                            dbContext.WirelessBoot_Devices.Remove(deviceWithIpFound);
                        }

                        // update ip and set IsHome to true
                        deviceFound.DeviceIpAddress = device.IpAddress;
                        deviceFound.DeviceIsHome = true;
                        deviceFound.DeviceName = device.Name;
                        dbContext.SaveChanges();
                        return true;
                    }

                    // if ip exists, update mac address, name and set IsHome to true
                    deviceFound = dbContext.WirelessBoot_Devices.FirstOrDefault(x => string.Equals(x.DeviceIpAddress, device.IpAddress));
                    if (deviceFound != null)
                    {
                        deviceFound.DeviceIpAddress = device.IpAddress;
                        deviceFound.DeviceIsHome = true;
                        deviceFound.DeviceName = device.Name;
                        dbContext.SaveChanges();
                        return true;
                    }

                    // if no to both, add device with IsHome to true
                    // Create a new db Item object to store into the queue
                    var deviceDb = new WirelessBoot_Devices()
                    {
                        DeviceIpAddress = device.IpAddress,
                        DeviceMacAddress = device.MacAddress,
                        DeviceName = device.Name,
                        DeviceFamilyMember = device.FamilyMember,
                        DeviceIsHome = true
                    };

                    // Insert our newly created Item and Submit the change to the db
                    dbContext.WirelessBoot_Devices.Add(deviceDb);
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                // Log error message
            }
            return false;
        }

        public static List<Device> GetAllDevices()
        {
            try
            {
                var devices = new List<Device>();
                using (var dbContext = new WirelessBootEntities())
                {
                    dbContext.WirelessBoot_Devices.ToList().ForEach(i =>
                        {
                            devices.Add(new Device()
                            {
                                Id = i.DeviceId,
                                Name = i.DeviceName,
                                MacAddress = i.DeviceMacAddress,
                                IpAddress = i.DeviceIpAddress,
                                IsHome = i.DeviceIsHome??false,
                                FamilyMember = i.DeviceFamilyMember
                            });
                        });
                }
                return devices;
            }
            catch (Exception ex)
            {
                // Log error message
            }
            return null;
        }

        public static bool RemoveDevice(int deviceId)
        {
            using (var dbContext = new WirelessBootEntities())
            {
                var device = dbContext.WirelessBoot_Devices.FirstOrDefault(i => i.DeviceId== deviceId);
                if (device != null)
                {
                    dbContext.WirelessBoot_Devices.Remove(device);
                    dbContext.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public static bool AssignDevice(int deviceId, string familyMember)
        {
            using (var dbContext = new WirelessBootEntities())
            {
                var device = dbContext.WirelessBoot_Devices.FirstOrDefault(i => i.DeviceId == deviceId);
                if (device != null)
                {
                    device.DeviceFamilyMember = familyMember;
                    dbContext.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public static bool UnassignDevice(int deviceId)
        {
            using (var dbContext = new WirelessBootEntities())
            {
                var device = dbContext.WirelessBoot_Devices.FirstOrDefault(i => i.DeviceId == deviceId);
                if (device != null)
                {
                    device.DeviceFamilyMember = string.Empty;
                    dbContext.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public static bool MarkDeviceHome(int deviceId, bool isHome)
        {
            using (var dbContext = new WirelessBootEntities())
            {
                var device = dbContext.WirelessBoot_Devices.FirstOrDefault(i => i.DeviceId == deviceId);
                if (device != null)
                {
                    device.DeviceIsHome = isHome;
                    dbContext.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public static void MarkAllDevicesAway()
        {
            using (var dbContext = new WirelessBootEntities())
            {
                dbContext.WirelessBoot_Devices.ToList().ForEach(i =>
                {
                    i.DeviceIsHome = false;
                });
                dbContext.SaveChanges();
            }
        }
    }
}