﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WirelessBootService.Models;

namespace WirelessBootService
{
    [ServiceContract]
    public interface IWirelessBootService
    {
        [OperationContract]
        void AddDevice(Stream deviceStream);

        [OperationContract]
        bool RemoveDevice(int deviceId);

        [OperationContract]
        List<Device> GetAllDevices();

        [OperationContract]
        bool AssignDevice(int deviceId, string familyMember);

        [OperationContract]
        bool UnassignDevice(int deviceId);

        [OperationContract]
        void MarkAllDevicesAway();
    }
}
