﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Script.Serialization;
using WirelessBootService.Data;
using WirelessBootService.Models;

namespace WirelessBootService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WirelessBootService : IWirelessBootService
    {
        [WebInvoke(Method = "POST", UriTemplate = "AddDevice", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public void AddDevice(Stream deviceStream)
        {
            try
            {
                // Deserialize json string coming in to an Item
                var streamReader = new StreamReader(deviceStream);
                var streamStr = streamReader.ReadToEnd();
                var item = new JavaScriptSerializer().Deserialize<Device>(streamStr);

                if (!WirelessBootDataManager.AddDevice(item)) throw new Exception("Failed to add new Item.");
            }
            catch (Exception ex)
            {
                // Log error message
            }
        }

        [WebGet(UriTemplate = "RemoveDevice?deviceId={deviceId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool RemoveDevice(int deviceId)
        {
            return WirelessBootDataManager.RemoveDevice(deviceId);
        }

        /// <summary>
        /// Get all unread Item messages
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="deviceId"></param>
        /// <param name="deviceType"></param>
        /// <returns></returns>
        [WebGet(UriTemplate = "GetAllDevices", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<Device> GetAllDevices()
        {
            // Get all unread messages
            return WirelessBootDataManager.GetAllDevices();
        }

        [WebGet(UriTemplate = "AssignDevice?deviceId={deviceId}&familyMember={familyMember}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool AssignDevice(int deviceId, string familyMember)
        {
            return WirelessBootDataManager.AssignDevice(deviceId, familyMember);
        }

        [WebGet(UriTemplate = "UnassignDevice?deviceId={deviceId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool UnassignDevice(int deviceId)
        {
            return WirelessBootDataManager.UnassignDevice(deviceId);
        }

        [WebGet(UriTemplate = "MarkAllDevicesAway", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public void MarkAllDevicesAway()
        {
            WirelessBootDataManager.MarkAllDevicesAway();
        }
    }
}
