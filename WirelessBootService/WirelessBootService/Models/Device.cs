﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WirelessBootService.Models
{
    public class Device
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string IpAddress { get; set; }

        public string MacAddress { get; set; }

        public string FamilyMember { get; set; }

        public bool IsHome { get; set; }
    }
}