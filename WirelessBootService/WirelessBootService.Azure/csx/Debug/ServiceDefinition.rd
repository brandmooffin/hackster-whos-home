﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="WirelessBootService.Azure" generation="1" functional="0" release="0" Id="bbb1401d-89c2-433b-86cb-0934e0bea9d9" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="WirelessBootService.AzureGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="WirelessBootService:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/WirelessBootService.Azure/WirelessBootService.AzureGroup/LB:WirelessBootService:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="WirelessBootService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/WirelessBootService.Azure/WirelessBootService.AzureGroup/MapWirelessBootService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="WirelessBootServiceInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/WirelessBootService.Azure/WirelessBootService.AzureGroup/MapWirelessBootServiceInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:WirelessBootService:Endpoint1">
          <toPorts>
            <inPortMoniker name="/WirelessBootService.Azure/WirelessBootService.AzureGroup/WirelessBootService/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapWirelessBootService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/WirelessBootService.Azure/WirelessBootService.AzureGroup/WirelessBootService/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapWirelessBootServiceInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/WirelessBootService.Azure/WirelessBootService.AzureGroup/WirelessBootServiceInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="WirelessBootService" generation="1" functional="0" release="0" software="C:\Projects\Wireless Boot\WirelessBootService\WirelessBootService.Azure\csx\Debug\roles\WirelessBootService" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;WirelessBootService&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;WirelessBootService&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/WirelessBootService.Azure/WirelessBootService.AzureGroup/WirelessBootServiceInstances" />
            <sCSPolicyUpdateDomainMoniker name="/WirelessBootService.Azure/WirelessBootService.AzureGroup/WirelessBootServiceUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/WirelessBootService.Azure/WirelessBootService.AzureGroup/WirelessBootServiceFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="WirelessBootServiceUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="WirelessBootServiceFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="WirelessBootServiceInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="bb73641a-00d8-4aad-9868-036e895a9454" ref="Microsoft.RedDog.Contract\ServiceContract\WirelessBootService.AzureContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="db310fde-0204-4f42-9fd6-55a566e94bbb" ref="Microsoft.RedDog.Contract\Interface\WirelessBootService:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/WirelessBootService.Azure/WirelessBootService.AzureGroup/WirelessBootService:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>