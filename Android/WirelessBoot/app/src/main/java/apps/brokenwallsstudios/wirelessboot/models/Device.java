package apps.brokenwallsstudios.wirelessboot.models;

/**
 * Created by brand on 11/9/2015.
 */
public class Device {
    public int Id;
    public String Name;
    public String IpAddress;
    public String MacAddress;
    public String FamilyMember;
    public boolean IsHome;
}
